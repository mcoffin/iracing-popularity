.DEFAULT_GOAL := addon

js_sources := $(shell find src -type f)
PACKAGE_NAME ?= $(shell jq -r '.name' < package.json | tr -d '\n')
dist_file := dist/$(PACKAGE_NAME).js

clean_folders := dist

ifneq ($(CLEAN_MODULES),)
	clean_folders += node_modules
endif

%.json: %.yml node_modules
	node ./bin/yaml2json.mjs $< | jq > $@

dist:
	[ -d $@ ] || mkdir -p $@

node_modules: package.json package-lock.json
	npm i

$(dist_file): dist $(shell find src -type f) node_modules
	npm run webpack-production

addon: manifest.json $(dist_file)

$(PACKAGE_NAME).tar.xz: $(dist_file) manifest.json
	[ ! -e $@ ] || rm $@
	tar -cJf $@ dist/ manifest.json

package: $(PACKAGE_NAME).tar.xz

clean:
	[ ! -e $(PACKAGE_NAME).tar.xz ] || rm -f $(PACKAGE_NAME).tar.xz
	[ ! -e manifest.json ] || rm -f manifest.json
	$(foreach d,$(clean_folders),[ ! -e $(d) ] || rm -rf $(d);)

clean-modules:
	[ ! -e node_modules ] || rm -rf node_modules

.PHONY: clean addon package
