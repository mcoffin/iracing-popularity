import _ from 'lodash';
import YAML from 'yaml';
import minimist from 'minimist';
import { streamToString } from '@mcoffin/stream-utils';
import fs from 'fs';
import MinimistConfig from '@mcoffin/minimist-config';

function isNullOrUndefined(v) {
    return _.isNull(v) || _.isUndefined(v);
}

class Config extends MinimistConfig {
    constructor(args) {
        super({
            alias: {
                reverse: ['r'],
            },
            boolean: [
                'reverse',
            ],
        }, args);
    }

    get reverse() {
        return this.get('reverse', false);
    }

    get streams() {
        let files = this.trailing;
        if (files.length <= 0) {
            return [process.stdin];
        }
        return files.map((filename) => {
            if (filename === '-') {
                return process.stdin;
            } else {
                return fs.createReadStream(filename)
            }
        });
    }
}

async function convertStream(stream, reverse = false) {
    const [ parse, stringify ] = reverse ? [ JSON.parse, YAML.stringify ] : [ YAML.parse, JSON.stringify ];
    const s = await streamToString(stream, 'utf8');
    const v = parse(s);
    return stringify(v);
}

async function main(config) {
    function printStream(stream) {
        return convertStream(stream, config.reverse)
            .then((s) => console.log(s));
    }
    const promises = config.streams.map(printStream);
    return await Promise.all(promises);
}

const config = new Config(process.argv.slice(2));
main(config);
