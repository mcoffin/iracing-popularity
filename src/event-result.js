const _ = require('lodash');
const Lazy = require('lazy.js');
const { timeoutPromise } = require('@mcoffin/promise-utils');
const { awaitQuerySelector, querySelectorAllArray } = require('./query-selector');
const { getElementText, setElementText } = require('./element');
const { createLogFunction } = require('./logging');
const sideEffect = require('@mcoffin/side-effect');
const { parseLapTime, formatLapTime } = require('./laptime');
const { getMedian } = require('./math-utils');

function isNullOrUndefined(v) {
    return _.isNull(v) || _.isUndefined(v);
}

async function setupMulticlassButtons() {
    const buttons = await awaitQuerySelector('div.multiClassButton', { waitTime: 1000 });
    buttons.forEach((button) => {
        button.addEventListener('click', () => doLaptimeAverages(getElementText(button)));
    });
    return buttons;
}

function getCell(idx) {
    const cells = !_.isUndefined(this.cells) ? Array.from(this.cells) : querySelectorAllArray.bind(this)('tr > td');
    if (!_.isNumber(idx)) {
        return cells;
    } else {
        return cells[idx];
    }
}

class ResultRow {
    constructor(element, sessionType = 'RACE') {
        this.element = element;
        this.setSessionType(sessionType);
    }

    setSessionType(sessionType) {
        this.sessionType = sessionType;
        switch (sessionType) {
            case 'RACE':
                this.indices = {
                    position: 0,
                    classPosition: 1,
                    incidents: 15,
                };
                break;
            case 'QUALIFY':
                this.indices = {
                    position: 0,
                    classPosition: 1,
                    incidents: 13,
                };
                break;
            default:
                throw new Error(`invalid sessionType: "${sessionType === undefined ? 'undefined' : sessionType}"`);
                break;
        }
    }

    getCell(...args) {
        return getCell.bind(this.element)(...args);
    }

    numberAtIndex(idx) {
        function getText() {
            return this.textContent || this.text;
        }
        const cell = this.getCell(idx);
        if (this.sessionType = 'QUALIFY' && idx === 13) {
            console.log('QUALIFY incidents cell:', cell);
        }
        try {
            const ret = parseInt(getText.bind(cell)().trim());
            if (_.isString(ret)) {
                return ret.trim();
            } else {
                return ret;
            }
        } catch(e) {
            console.error(e);
            return -1;
        }
    }

    getCellIndex(name, defaultValue) {
        return this.indices[name] || defaultValue;
    }

    get position() {
        return this.numberAtIndex(this.getCellIndex('position', 0));
    }

    get classPosition() {
        return this.numberAtIndex(this.getCellIndex('classPosition', 1));
    }

    get incidents() {
        const idx = _.get(this.indices, 'incidents', 15);
        if (this.sessionType === 'QUALIFY') {
            console.log('QUALIFY incident idx:', idx);
        }
        return this.numberAtIndex(_.get(this.indices, 'incidents', 15));
    }
}

async function getEventTables(title) {
    function getTitle(el) {
        const titleElement = el.querySelector('table.event_table th > h1');
        if (_.isUndefined(titleElement)) {
            return undefined;
        }
        return getElementText(titleElement);
    }
    return await awaitQuerySelector('table.event_table', {
        filter(table) {
            return _.isUndefined(title) || getTitle(table) === title;
        },
    });
}

function getFastLapIndex(sessionType) {
    switch (sessionType) {
        case 'RACE':
            return 12;
            break;
        case 'QUALIFY':
            return 10;
            break;
        default:
            throw new Error(`invalid sessionType: "${sessionType}"`);
            break;
    }
}

async function doLaptimeAverages(carClass = 'ALL', session = 'RACE') {
    const loggerPrefix = `doLaptimeAverages(${session !== undefined ? session : 'undefined'})`;
    const doLog = createLogFunction(loggerPrefix);
    const appendLog = createLogFunction('appendTitleBlock', doLog);
    const doError = createLogFunction(loggerPrefix, console.error);
    doLog(`laptime averages start for class: ${carClass} in session: ${session}`);
    const tables = await getEventTables(session);
    if (tables.length <= 0) {
        doError('No tables found');
        return;
    }
    const promises = tables.map(async (table) => {
        const fastLapIndex = getFastLapIndex(session);
        doLog(`fastLapIndex[${session !== undefined ? session : 'undefined'}]:`, fastLapIndex);
        const rows = await awaitQuerySelector.bind(table)('tr[id^="race_row"]');
        const fastLapCells = rows.map((row) => getCell.bind(row)(fastLapIndex));
        function getLapTimes(cells) {
            return Lazy(cells)
                .map(getElementText)
                .map((s) => s.trim())
                .map((s) => {
                    try {
                        return parseLapTime(s);
                    } catch (e) {
                        doError(e);
                        return undefined;
                    }
                })
                .filter(_.toNumber)
                .toArray();
        }
        const getAverage = (a) => Lazy(a).sum() / a.length;
        let lapTimes = getLapTimes(fastLapCells)
        doLog('lapTimes:', lapTimes);
        let averageLapTime = getAverage(lapTimes);
        let medianLapTime = getMedian.bind(Lazy(lapTimes).sort().toArray())();
        doLog('averageLapTime:', averageLapTime);
        doLog('medianLapTime:', medianLapTime);
        const titleElement = table.querySelector('th > h1');
        function appendTitleBlock(s) {

            appendLog('titleBlock:', s);
            titleElement.textContent = getElementText(titleElement) + ` (${s})`;
        }
        doLog('titleElement:', titleElement);
        let titleText = getElementText(titleElement);
        doLog('beforeText:', titleText);
        titleText = titleText + ` (average: ${formatLapTime(averageLapTime)} [${formatLapTime(medianLapTime)}])`;
        doLog('textToSet', titleText);
        setElementText(titleElement, titleText);
        doLog('afterText:', getElementText(titleElement));
        const podiumLapCells = Lazy(rows)
            .map((cell) => new ResultRow(cell))
            .filter((row) => row.classPosition >= 0 && row.classPosition <= 3)
            .map((row) => row.getCell(fastLapIndex))
            .toArray();
        doLog('podiumLapCells:', podiumLapCells);
        lapTimes = getLapTimes(podiumLapCells);
        averageLapTime = getAverage(lapTimes);
        doLog('averagePodiumLapTime', averageLapTime);
        if (_.isNumber(averageLapTime)) {
            appendTitleBlock(`podium: ${formatLapTime(averageLapTime)}`);
            // titleElement.textContent = getElementText(titleElement) + ` (podium: ${formatLapTime(averageLapTime)})`;
        }
        const topFiveCells = Lazy(rows)
            .map((cell) => new ResultRow(cell))
            .filter((row) => row.classPosition >= 0 && row.classPosition <= 5)
            .map((row) => row.getCell(fastLapIndex))
            .toArray();
        doLog('topFiveCells:', topFiveCells);
        lapTimes = getLapTimes(topFiveCells);
        averageLapTime = getAverage(lapTimes);
        doLog('averageTopFiveLapTime:', averageLapTime);
        if (_.isNumber(averageLapTime)) {
            appendTitleBlock(`top five: ${formatLapTime(averageLapTime)}`);
            // titleElement.textContent = getElementText(titleElement) + ` (top five: ${formatLapTime(averageLapTime)})`;
        }

        let incidents = Lazy(rows)
            .map((cell) => new ResultRow(cell, session))
            .map((row) => {
                doLog('incident row:', row);
                try {
                    doLog('row incidents:', row.incidents);
                    return row.incidents;
                } catch(e) {
                    doError('row had a bad incidents cell:', row);
                    return undefined;
                }
            })
            .filter(_.isNumber)
            .sort()
            .toArray();
        doLog('incidents:', incidents);
        let averageIncidents = Lazy(incidents).sum() / rows.length;
        averageIncidents = averageIncidents.toFixed(1);
        const medianIncidents = getMedian.bind(incidents)();
        doLog('averageIncidents:', averageIncidents);
        doLog('medianIncidents:', medianIncidents);
        if (!isNullOrUndefined(averageIncidents)) {
            let titleBlock = `incidents ${averageIncidents}`;
            if (!isNullOrUndefined(medianIncidents)) {
                titleBlock = titleBlock + ` [${medianIncidents}]`;
            }
            appendTitleBlock(titleBlock);
        }
    });
    return await Promise.all(promises);
}

console.log('event-result.js loaded');
setupMulticlassButtons()
    .then(sideEffect((buttons) => console.log('set up multiclass buttons:', buttons)));
doLaptimeAverages()
    .then((result) => console.log('laptime averages done:', result));
doLaptimeAverages('ALL', 'QUALIFY')
    .then((result) => console.log('laptime averages done:', result));
