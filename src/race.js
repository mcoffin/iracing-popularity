const _ = require('lodash');

function getElementText(elem) {
    return elem.text || elem.textContent;
}

class RaceCell {
    constructor(element) {
        this.element = element;
    }

    get contentElement() {
        const children = Array.from(this.element.childNodes);
        return children[0];
    }

    get textContent() {
        const el = this.contentElement;
        if (_.isUndefined(el)) {
            return undefined;
        } else {
            return getElementText(el);
        }
    }
}

class Race {
    constructor(element) {
        this.element = element;
    }

    get childNodes() {
        return Array.from(this.element.childNodes);
    }

    get startTime() {
        const ret = this.textAtIndex(0);
        console.log('Race::startTime: element:', ret);
        return _.isString(ret) && ret.length > 0 ? ret : undefined;
    }

    get carClass() {
        return this.textAtIndex(3);
    }

    get fieldSize() {
        return this.numberAtIndex(4);
    }

    get strengthOfField() {
        return this.numberAtIndex(5);
    }

    textAtIndex(idx) {
        const cell = new RaceCell(this.childNodes[idx]);
        return cell.textContent;
    }

    numberAtIndex(idx) {
        let v = this.textAtIndex(idx);
        if (_.isString(v)) {
            v = parseInt(v);
        }
        return v;
    }
}

module.exports = {
    Race,
    RaceCell,
};
