const { timeoutPromise } = require('@mcoffin/promise-utils');
const { querySelectorAllArray } = require('./query-selector');

function getRatingFieldInternal(ratingType = 'road') {
    const containerQuery = `.license-container.${ratingType}-container`;
    return querySelectorAllArray(`${containerQuery} > span.irating-field`)[0];
}

async function getRatingField(ratingType = 'road', interval = 500) {
    let ratingField = getRatingFieldInternal(ratingType);
    while (!ratingField) {
        await timeoutPromise(interval);
        ratingField = getRatingFieldInternal(ratingType);
    }
    return ratingField;
}

function getRating(...args) {
    return getRatingField(...args)
        .then((f) => f.textContent || '-1')
        .then((s) => parseInt(s));
}

module.exports = {
    getRatingField,
    getRating,
};
