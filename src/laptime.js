const _ = require('lodash');
const { createLogFunction } = require('./logging');

const laptimePattern = new RegExp("^([0-9]+):([0-9]+).([0-9]+)$");

class InvalidLapTimeError extends Error {
    constructor(lapTime, result) {
        super(`Invalid lap time: ${lapTime}`);
        this.lapTime = lapTime;
        this.result = result;
    }
}

function parseLapTime(s) {
    if (_.isNumber(s)) {
        return s;
    } else if (s === '-') {
        return undefined;
    } else if (_.isString(s)) {
        const matches = laptimePattern.exec(s);
        if (!_.isArray(matches)) {
            throw new InvalidLapTimeError(s, matches);
        }
        const [ minutes, seconds, subseconds ] = matches.slice(1).map((s) => parseInt(s));
        let lapTime = 0;
        lapTime += minutes * 60;
        lapTime += seconds;
        lapTime += parseFloat(`0.${subseconds}`);
        return lapTime;
    }
}

function formatLapTime(seconds) {
    console.log(`formatLapTime(${seconds})`);
    const minutes = Math.floor(seconds / 60);
    seconds = seconds % 60;
    return `${minutes}:${seconds.toFixed(3)}`;
}

module.exports = {
    InvalidLapTimeError,
    parseLapTime,
    formatLapTime,
};
