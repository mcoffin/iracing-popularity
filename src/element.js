function getText() {
    return this.textContent || this.text;
}

function setText(s) {
    if (_.isString(this.textContent)) {
        this.textContent = s;
    } else {
        this.text = s;
    }
}

function getElementText(el) {
    return getText.bind(el)();
}

function setElementText(el, s) {
    return setText.bind(el)(s);
}

module.exports = {
    getElementText,
    setElementText,
};
