const _ = require('lodash');
const Lazy = require('lazy.js');
const { timeoutPromise } = require('@mcoffin/promise-utils');
const { querySelectorAllArray } = require('./query-selector');
const { Race } = require('./race');
const { getElementText } = require('./element');
const { stringifySafe } = require('./util');
const { logSafe, logErrorSafe } = require('./logging');
const { getRating } = require('./rating');

function getRaceHeaders() {
    return querySelectorAllArray('[class*="raceHeader"]').map((elem) => new Race(elem));
}

async function setupWeekSelector() {
    const getWeekSelector = () => querySelectorAllArray('#weekSelect')[0];
    let weekSelector = getWeekSelector();
    while (!weekSelector) {
        await timeoutPromise(500);
        weekSelector = getWeekSelector();
    }
    weekSelector.addEventListener('change', () => {
        console.log('weekSelector changed!');
        main();
    });
    return weekSelector;
}

class HeaderElement {
    constructor(el) {
        this.element = el;
        this.elementStorageKey = 'iracingPopularity';
    }

    set element(el) {
        this._element = el;
        const elementOriginalText = this.getFromElement('originalText');
        this.originalText = _.isString(elementOriginalText) ? elementOriginalText : getElementText(el);
        this.storeOnElement('originalText', this.originalText);
    }

    getFromElement(k, ...args) {
        return _.get(this.element, `${this.elementStorageKey}.${k}`, ...args);
    }

    get elementStorage() {
        if (!_.isObject(this.element[this.elementStorageKey])) {
            this.element[this.elementStorageKey] = {};
        }
        return this.element[this.elementStorageKey];
    }

    storeOnElement(k, v) {
        this.elementStorage[k] = v;
    }

    isModified() {
        return _.isString(this.getFromElement('originalText'));
    }

    get element() {
        return this._element;
    }

    get textContent() {
        return getElementText(this.element);
    }

    set textContent(s) {
        this.element.textContent = s;
    }

    appendBlock(s, delimeters = ['(', ')']) {
        if (_.isUndefined(s)) {
            return;
        }
        const [open, close] = Lazy(delimeters)
            .map(stringifySafe)
            .map((s) => s === 'undefined' ? '' : s)
            .toArray();
        let prefix = this.textContent;
        if (_.isString(prefix) && prefix.length > 0) {
            prefix += ' ';
        }
        const blockText = `${open}${stringifySafe(s)}${close}`;
        this.element.textContent = prefix + blockText;
    }

    reset() {
        if (!_.isString(this.originalText)) {
            throw new Error(`Invalid originalText: ${this.originalText}`);
        }
        this.elementText = this.originalText;
    }
}

function groupRaces(races = []) {
    const ret = {};
    let startTime = 'undefined';
    races.forEach((race) => {
        console.log('groupRaces: race:', race);
        if (_.isUndefined(race.startTime)) {
            if (!_.isArray(ret[startTime])) {
                ret[startTime] = [];
            }
            ret[startTime].push(race);
        } else {
            startTime = race.startTime;
        }
    });
    return ret;
}

async function main(firstRun = false) {
    function isValid() {
        if (getRaceHeaders().length < 1) {
            return false;
        }
        if (!querySelectorAllArray('#weekSelect')[0]) {
            return false;
        }
        return true;
    }
    await timeoutPromise(1000);
    while (!isValid()) {
        await timeoutPromise(5000);
    }

    const header = new HeaderElement(querySelectorAllArray('table.series_results_table div.euro')[0]);
    console.log('resultsHeader:', header.element);
    if (header.isModified()) {
        logSafe('warning: header was modified. resetting to:', header.originalText);
        header.reset();
    }

    const races = getRaceHeaders();
    console.log('races:', races);
    const groupedRaces = groupRaces(races);
    console.log('groupedRaces:', groupedRaces);
    const myRating = await getRating('road');
    console.log(`myRating: ${myRating}`);
    const closestRaces = Lazy(groupedRaces)
        .pairs()
        .map(([k, v]) => {
            const racesByClass = Lazy(v)
                .groupBy((race) => race.carClass)
                .toObject();
            console.log('racesByClass:', racesByClass);
            const closestRaces = Lazy(racesByClass)
                .pairs()
                .map(([carClass, races]) => {
                    console.log('carClass:', carClass);
                    console.log('races:', races);
                    const sortedRaces = Lazy(races)
                        .sortBy((race) => Math.abs(race.strengthOfField - myRating))
                        .toArray();
                    return [carClass, sortedRaces[0] || undefined];
                })
                .filter(([k, v]) => !_.isUndefined(v))
                .toObject();
            return [k, closestRaces];
        })
        .map(([k, v]) => Object.values(v))
        .flatten()
        .each((race) => {
            race.element.style = "background-color: yellow;";
        });
    const entries = {};
    races.forEach((race) => {
        let entryCount = _.get(entries, race.carClass, 0);
        entryCount += race.fieldSize;
        entries[race.carClass] = entryCount;
    });
    console.log('entries:', entries);
    const totalEntries = Lazy(entries)
        .values()
        .sum();
    console.log('totalEntries:', totalEntries);

    header.appendBlock(`totalEntries: ${totalEntries}`);
    Lazy(entries)
        .pairs()
        .map(([k, v]) => `${k}: ${v} ${(v / totalEntries * 100.0).toFixed(2)}%`)
        .each((s) => _.isString(s) ? header.appendBlock(s) : console.error(`warning: non-string block: ${s}`));
}
main(true);
setupWeekSelector()
    .then((el) => console.log('week selector set up:', el));
