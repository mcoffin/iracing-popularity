const _ = require('lodash');
const { timeoutPromise } = require('@mcoffin/promise-utils');
const { createLogFunction } = require('./logging');

function querySelectorAll() {
    return document.querySelectorAll(...arguments);
}

function getDefaultThis() {
    return this;
}

function stringify(v, doStringForValue = true) {
    if (_.isUndefined(v)) {
        return 'undefined';
    } else if (_.isNull(v)) {
        return 'null';
    } else {
        return doStringForValue ? `${v}` : v;
    }
}

function nop() {
}

function querySelectorAllArray(...selectors) {
    // const doLog = createLogFunction('querySelectorAllArray');
    // const doError = createLogFunction('querySelectorAllArray', console.error);
    const [ doLog, doError ] = [nop, nop];
    doLog('selectors:', selectors);
    doLog('this:', stringify(this, false));
    const base = (() => {
        if (_.isUndefined(this) || _.isNull(this) || this === getDefaultThis()) {
            return document;
        } else {
            return this;
        }
    })();
    doLog('base:', stringify(base, false));
    if (!_.isFunction(base.querySelectorAll)) {
        doError('invalid base:', stringify(base, false));
    } else {
        doLog('base.querySelectorAll', base.querySelectorAll);
    }
    const list = base.querySelectorAll(...selectors);
    doLog('list:', list);
    return Array.from(list);
}

async function awaitQuerySelector(selectors, options = {}) {
    const { waitTime, minCount, filter } = _.defaults(options, {
        waitTime: 500,
        minCount: 1,
        filter: () => true,
    });
    if (_.isString(selectors)) {
        selectors = [selectors];
    }
    console.log(`awaitQuerySelector(this = ${stringify(this)}, selectors = ${selectors})`);
    const doQuery = querySelectorAllArray.bind(this);
    const getElements = () => {
        const ret = doQuery(...selectors);
        console.log(`elements for query "${selectors.join(', ')}":`, ret);
        return _.filter(ret, filter);
    };
    let elements = getElements();
    while (elements.length < minCount) {
        await timeoutPromise(waitTime);
        elements = getElements();
    }
    return elements;
}

module.exports = {
    querySelectorAll,
    querySelectorAllArray,
    awaitQuerySelector,
};
