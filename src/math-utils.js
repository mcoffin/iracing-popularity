const _ = require('lodash');

function getMedian(truncate = true) {
    if (!_.isArray(this) || this.length < 1) {
        return undefined;
    } else if (this.length === 1) {
        return this[0];
    }

    function truncateIdx(idx) {
        if (truncate) {
            return Math.floor(idx);
        } else {
            return Math.ceil(idx);
        }
    }

    const medianIdx = truncateIdx((this.length - 1) / 2);
    return this[medianIdx];
}

module.exports = {
    getMedian,
};
