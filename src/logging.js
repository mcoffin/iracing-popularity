const _ = require('lodash');
const { stringifySafe } = require('./util');

function doLogSafe(logFn, ...args) {
    if (!_.isFunction(logFn)) {
        logFn = console.log;
    }
    args = args.map(stringifySafe);
    return logFn(...args);
}

function logSafe(...args) {
    doLogSafe(console.log, ...args);
}

function logErrorSafe(...args) {
    doLogSafe(console.error, ...args);
}

exports.logSafe = logSafe;
exports.logErrorSafe = logErrorSafe;

exports.createLogFunction = function createLogFunction(name, logFn = console.log) {
    return function log(...args) {
        console.log(`${name}:`, ...args);
    };
};
