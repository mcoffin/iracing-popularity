const _ = require('lodash');

function stringifySafe(v) {
    return _.isUndefined(v) ? 'undefined' : `${v}`;
}

module.exports = {
    stringifySafe,
};
