const _ = require('lodash');
const path = require('path');
const fs = require('fs');
const { DefinePlugin } = require('webpack');
const TerserPlugin = require('terser-webpack-plugin');

const mode = process.env['NODE_ENV'] || 'development';

function isPresentNonZeroString(v) {
    return _.isString(v) && v.length > 0 && v !== "0";
}

function getDevtool(mode) {
    switch (mode) {
        case 'production':
            return undefined;
            break;
        default:
            return 'eval-source-map';
            break;
    }
}

function getOptimizations(mode) {
    switch (mode) {
        case 'production':
            return {
                minimize: true,
                minimizer: [new TerserPlugin()],
            };
            break;
        default:
            return {};
            break;
    }
}

class Package {
    constructor(p = __dirname) {
        this.path = p;
    }

    set path(p) {
        this._path = p;
        p = path.join(p, 'package.json');
        this.config = JSON.parse(fs.readFileSync(p, { encoding: 'utf8' }));
    }

    get path() {
        return this._path;
    }

    get(key, defaultValue) {
        return _.get(this.config, key, defaultValue);
    }

    get name() {
        return this.get('name');
    }
}

const pkg = new Package(__dirname);

const entry = {};
entry[pkg.name] = './src/index.js';
entry['event-result'] = './src/event-result.js';

module.exports = {
    mode: mode,
    entry,
    output: {
        filename: "[name].js",
        publicPath: "/",
    },
    module: {
        rules: [
            {
                test: /\.ya?ml$/,
                use: [
                    'json-loader',
                    'yaml-loader'
                ],
            },
            {
                test: /\.json$/,
                loader: 'json-loader',
                include: path.resolve(__dirname, 'src')
            }
        ],
        exprContextRegExp: true,
    },
    optimization: _.merge({
        // splitChunks: {
        //     cacheGroups: {
        //         commons: {
        //             test: /[\\/]node_modules[\\/]/,
        //             name(module, chunks, cacheGroupKey) {
        //                 const moduleFilename = module.identifier().split('/').reduceRight(item => item);
        //                 const allChunksNames = chunks.map(_.property('name')).join('~');
        //                 return `${cacheGroupKey}-${allChunksNames}-${moduleFilename}`;
        //             },
        //             chunks: 'all'
        //         },
        //     },
        // },
    }, getOptimizations(mode)),
    devtool: getDevtool(mode),
    plugins: [
        new DefinePlugin({
            'process.env.NODE_ENV': JSON.stringify(mode),
        }),
    ],
};
